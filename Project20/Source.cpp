#include"person.h"
#include<iostream>
#include<io.h>
#include <string>
#include "sqlite3.h"
using namespace std;
persons personList;
int callback(void *data, int argc, char **argv, char **azColName)
{
	Person person;
	for (int i = 0; i < argc; i++) {
		if (string(azColName[i]) == "ID") {
			person.setId(atoi(argv[i]));
		}
		else if (string(azColName[i]) == "LAST_NAME") {
			person.setLastName(argv[i]);
		}
		else if (string(azColName[i]) == "FIRST_NAME") {
			person.setFirstName(argv[i]);
		}
		else if (string(azColName[i]) == "EMAIL") {
			person.setEmail(argv[i]);
		}
	}
	personList.push_back(person);
	return 0;
}

void print()
{
	persons_iter iter = personList.begin();
	while (iter != personList.end())
	{
		cout << iter->getLastName() << " , "
			<< iter->getFirstName() << " , "
			<< iter->getEmail() << endl;	
		++iter;
	}
}

int main()
{
	
	
	char* sqlStatement = "CREATE TABLE PERSONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LAST_NAME TEXT NOT NULL, FIRST_NAME TEXT NOT	NULL, EMAIL TEXT NOT NULL); ";
	char* sqlStatement2 = "CREATE TABLE Phone (PhoneID INTEGER PRIMARY KEY, PhonePerfixID INTEGER,PhoneNumber,PersonID INTEGER, FOREIGN KEY (PersonID) REFERENCES PERSONS(id)); ";
	char* sqlStatement3 = "CREATE TABLE PhonePerfix (PhonePerfixID INTEGER,Perfix , FOREIGN KEY (PhonePerfixID) REFERENCES Phone(PhonePerfixID)); ";
	char* insert1 = "INSERT INTO PhonePerfix VALUES (1,'02'),(2,'03'),(3,'04'),(4,'08'),(5,'09'),(6,'050'),(7,'052'),(8,'053'),(9,'054'),(10,'055'),(11,'073'),(12,'077');";
	char* insert2 = "INSERT INTO PERSONS VALUES(1,'bilenki','aharon','aharonbilenki@gmail.com'),(2,'bilenki','aharon','aharonbilenki@gmail.com'),(3,'bilenki','aharon','aharonbilenki@gmail.com'),(4,'bilenki','aharon','aharonbilenki@gmail.com');";
	char* insert3 = "INSERT INTO Phone VALUES(1,2,'8295423',1),(2,3,'8295423',1),(3,1,'8295423',1),(4,6,'8295423',2),(5,2,'4535423',2),(6,2,'8266423',3);";
	char* insert4 = "INSERT INTO PhonePerfix VALUES (13,'089');";
	char* update1 = "UPDATE PhonePerfix set perfix = '078' where PhonePerfixID = 13;";
	char* update2 = "UPDATE PERSONS set LAST_NAME = 'Elimelech' where ID = 1;";
	char* update3 = "UPDATE PERSONS set FIRST_NAME='Sonia'  where ID = 1";
	char* delet1 = "DELETE FROM Phone WHERE PhoneID = 3;";
	char* delet2 = "DELETE FROM PERSONS WHERE ID = 2;";
	char* select1 = "SELECT * FROM PERSONS where PERSONS.FIRST_NAME='aharon'";

	char **errMessage = nullptr;
	string name = "";
	sqlite3* db;
	string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		cout << "Failed to open DB" << endl;
		return -1;
	}
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;

	res = sqlite3_exec(db, sqlStatement2, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;

	res = sqlite3_exec(db, sqlStatement3, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, insert1, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, insert2, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, insert3, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, insert4, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, update1, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, update2, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, update3, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, delet1, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, delet2, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	//cout << "enter name to select" << endl;
	//cin >> name;
	res = sqlite3_exec(db, select1, callback, nullptr, errMessage);
	if (res != SQLITE_OK) {
		return false;
	}
	print();

	sqlite3_close(db);
	db = nullptr;
	system("PAUSE");
	return 0;
}
