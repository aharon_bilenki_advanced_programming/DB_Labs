#include<iostream>
#include"person.h"
using namespace std;
Person::Person()
{
	m_email = "";
	m_firstName = "";
	m_id = 0;
	m_lastName = "";
}
Person::Person(int id, string lastName, string firstName)
	: m_id(id), m_lastName(lastName), m_firstName(firstName)
{}
int Person::getId() const
{
	return m_id;
}
void Person::setId(int id)
{
	m_id = id;
}
string Person::getFirstName() const
{
	return m_firstName;
}
void Person::setFirstName(const string& firstName)
{
	m_firstName = firstName;
}
string Person::getLastName() const
{
	return m_lastName;
}
void Person::setLastName(const string& lastName)
{
	m_lastName = lastName;
}
string Person::getEmail() const
{
	return m_email;
}
void Person::setEmail(const string& email)
{
	m_email = email;
}